public class Fox{
	public int height;
	public String breed;
	public int age;
	
	public void rangeAge(){
		if(this.age > 0 && this.age < 2){
			System.out.println("I'm a child");
		}
		else if(this.age >= 2 && this.age <= 6){
			System.out.println("I'm an adult");
		}	
		else{
			System.out.println("I'm too old to live, I will died soon");
		}
	}
	public void saybreed(){
		if(this.breed.equals("red fox")){
			System.out.println("I'm a red fox breed");
		}	
		else{	
			System.out.println("I'm one of the rare fox species on earth");
		}
	}
}	